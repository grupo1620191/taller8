#include <unistd.h>
#include <stdlib.h>
#include "../include/slaballoc.h"
#include "../include/prueba.h"

void crear_persona(void *ref, size_t tamano){
    Persona *pers = (Persona *)ref;
    pers->edad = 22;
    pers->nombre = (char *)malloc(sizeof(char)*100);
}

void destruir_persona(void *ref, size_t tamano){
    Persona *pers = (Persona *)ref;
    pers->edad = -1;
    free(pers->nombre);
}

void crear_carro(void *ref, size_t tamano){
    Carro *car = (Carro *)ref;
    car->anio = 2019;
    car->kilometraje = 4351200;
}

void destruir_carro(void *ref, size_t tamano){
    Carro *car = (Carro *)ref;
    car->anio = -1;
    car->kilometraje = -1;
}

void crear_stdnt(void *ref, size_t tamano){
    Estudiante *stdnt = (Estudiante *)ref;
    stdnt->carrera = "Ingenieria en Ciencias Computacionales";
    int i = 0;
    for(i = 0; i< 100; i++){
    	stdnt->notas[i] = i;
    }
}

void destruir_stdnt(void *ref, size_t tamano){
    Estudiante *stdnt = (Estudiante *)ref;
    stdnt->carrera = NULL;
    int i = 0;
    for(i = 0; i< 100; i++){
    	stdnt->notas[i] = 0;
    }
}

int main(){
	SlabAlloc *slabAlloc1;

	constructor_fn constructor= crear_carro;
	destructor_fn destructor= destruir_carro;

	slabAlloc1 = crear_cache("SLABALLOC", sizeof(Carro), constructor, destructor);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);



	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);
	obtener_cache(slabAlloc1,0);

	obtener_cache(slabAlloc1,1);



	stats_cache(slabAlloc1);

	return 0;
}
