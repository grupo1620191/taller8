
#include<stdio.h>
#include"../include/slaballoc.h"
//#include"../include/objetos.h"



SlabAlloc *crear_cache(char *nombre ,size_t tamanioObjeto,constructor_fn construct,destructor_fn destruct){
  SlabAlloc *slab=(SlabAlloc *)malloc(sizeof(SlabAlloc));
  slab->nombre=nombre;
  slab->mem_ptr=(void *)malloc(tamanioObjeto*TAMANO_CACHE);
  slab->constructor=construct;
  slab->destructor=destruct;

  for(int i=0;i<tamanioObjeto*TAMANO_CACHE;i+=tamanioObjeto){

    construct(slab->mem_ptr+i,tamanioObjeto);
  }
  slab->cantidad_en_uso=0; //inicializar los elementos dentro del slaballoc

  slab->tamano_objeto=tamanioObjeto; //asignar tamaño del objeto

  slab->tamano_cache=TAMANO_CACHE;  // asignar tamanño del cache

  Slab* punteroSlab=(Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);

  for(int cont=0; cont<TAMANO_CACHE; cont++){
    Slab slab_cache;

    slab_cache.ptr_data = slab->mem_ptr+cont*tamanioObjeto;
    slab_cache.ptr_data_old = slab->mem_ptr+cont*tamanioObjeto;
    slab_cache.status=DISPONIBLE;

    *(punteroSlab+cont)=slab_cache;
  }
  slab->slab_ptr=punteroSlab;

  return slab;
}

void *obtener_cache(SlabAlloc *allocator, int crecer){
  SlabAlloc alloc = *allocator;
  Slab *slaab = allocator->slab_ptr;
  //bandera crecer = 0
  if (crecer == 0) {
    for(int i = 0; i < allocator->tamano_cache; i++){
      if((*(slaab+i)).status == 0){
        allocator->cantidad_en_uso++;
        (*(slaab+i)).status = 1;
        return (*(slaab+i)).ptr_data;
      }
    }
  // bandera crecer = 1
  }
  if (crecer == 1 && ((alloc.cantidad_en_uso)==(alloc.tamano_cache))){
    unsigned int tamObjeto = alloc.tamano_objeto;
    alloc.tamano_cache = alloc.tamano_cache*2;
    alloc.mem_ptr = realloc(alloc.mem_ptr, alloc.tamano_objeto * alloc.tamano_cache);
    alloc.slab_ptr = (Slab *)realloc(alloc.slab_ptr,sizeof(Slab)*alloc.tamano_cache);

    for (int i = tamObjeto * alloc.tamano_objeto; i < alloc.tamano_objeto*alloc.tamano_cache; i = i+alloc.tamano_objeto) {
      alloc.constructor(i+(alloc.mem_ptr), alloc.tamano_objeto);
    }
    //for (int j = 0; j < tamObjeto; j++) {
      //(j+(alloc->slab_ptr))->ptr_data_old = (j+(alloc->slab_ptr))->ptr_data;
    //}
    for (int k = 0; k < alloc.tamano_cache; k++) {
      if (k >= tamObjeto){
        Slab sb;
        sb.ptr_data = (alloc.mem_ptr) + (alloc.tamano_objeto*k);
        sb.ptr_data_old = (alloc.mem_ptr) + (alloc.tamano_objeto*k);
        sb.status = DISPONIBLE;
        *((alloc.slab_ptr)+k) = sb;
      } else {
        ((alloc.slab_ptr)+k)->ptr_data = (alloc.mem_ptr) + (k*alloc.tamano_objeto);
      }
    }

  }
  return NULL;
}

void devolver_cache(SlabAlloc *alloc, void *obj) {
  SlabAlloc slablloc= *alloc;
  Slab *slab= slablloc.slab_ptr;

  int i = 0;
  for(i = 0; i < slablloc.tamano_cache; i++) {
    if (obj==slab[i].ptr_data){
      slab[i].ptr_data = NULL;
      slab[i].status = DISPONIBLE;
      slablloc.cantidad_en_uso -= 1;
      slablloc.destructor(slablloc.mem_ptr + slablloc.tamano_objeto*i, slablloc.tamano_objeto);
      break;
    }
  }

}

void destruir_cache(SlabAlloc *cache) {
  //SlabAlloc slaballoc= *cache;
  if(cache->cantidad_en_uso == 0){
    //destruir objetos y desconectar slabs con los objetos
    int i = 0;
    for(i = 0; i < cache->tamano_cache; i++){
      cache->destructor(cache->mem_ptr + cache->tamano_objeto*i, cache->tamano_objeto);
    }
    //liberar mem_ptr y slab
    free(cache->slab_ptr);
    free(cache->mem_ptr);
    //hacer null o 0 al slab y liberarlo
    cache = NULL;
    free(cache);
  }
}

void stats_cache(SlabAlloc *cache){
  SlabAlloc slaballoc = *cache;
  printf("Nombre de cache: %s\n", slaballoc.nombre);
  printf("Cantidad de slabs: %d\n", slaballoc.tamano_cache);
  printf("Cantidad de slabs en uso: %d\n", slaballoc.cantidad_en_uso);
  printf("Tamano de objeto: %ld\n\n", slaballoc.tamano_objeto);
  printf("Direccion de cache: %p \n", slaballoc.mem_ptr);

  Slab *slabs= slaballoc.slab_ptr;
  int i = 0;
  for (i = 0; i < slaballoc.tamano_cache; i++) {
    if (slabs[i].status == DISPONIBLE){
      printf("Direccion ptr[%d].ptr_data: <%p>    <DISPONIBLE>,   ptr[%d].ptr_data_old: <%p>\n", i, slabs[i].ptr_data, i, slabs[i].ptr_data_old);
    } else {
    printf("Direccion ptr[%d].ptr_data: <%p>    <EN_USO>,   ptr[%d].ptr_data_old: <%p>\n", i, slabs[i].ptr_data, i, slabs[i].ptr_data_old);
    }
  }


}
