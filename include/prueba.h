typedef struct persona{
    int edad;
    char *nombre;
} Persona;

//Constructor
void crear_persona(void *ref, size_t tamano);

//Destructor
void destruir_persona(void *ref, size_t tamano);

typedef struct carro {
    int anio;
    long kilometraje;
} Carro;

//Constructor
void crear_carro(void *ref, size_t tamano);

//Destructor
void destruir_carro(void *ref, size_t tamano);

typedef struct estudiante{
    float notas[100];
    char *carrera;
} Estudiante;

//Constructor
void crear_estudiante(void *ref, size_t tamano);

//Destructor
void destruir_estudiante(void *ref, size_t tamano);
