all: bin/estatico bin/dinamico

bin/estatico: obj/prueba.o lib/libslaballoc.a
	gcc -g -static obj/prueba.o -o bin/pruebaestatico -Llib -lslaballoc

bin/dinamico: obj/prueba.o lib/libslaballoc.so
	gcc -g obj/prueba.o -o bin/pruebadinamico -Llib -lslaballoc

obj/prueba.o: src/prueba.c
	gcc -g -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o


obj/slaballoc.o: src/slaballoc.c
	gcc -g -Wall -c -Iinclude/ src/slaballoc.c -o obj/slaballoc.o

#agregue las reglas que necesite

lib/libslaballoc.a: obj/slaballoc.o
	ar rcs lib/libslaballoc.a obj/slaballoc.o

lib/libslaballoc.so:
	gcc -shared -fPIC -o lib/libslaballoc.so obj/slaballoc.o




.PHONY: clean
clean:
	rm bin/* obj/*.o lib/*


.PHONY: runStatic
runStatic: bin/pruebaestatico
	bin/pruebaestatico

.PHONY: runDynamic
runDynamic: 
	export LD_LIBRARY_PATH=./lib
	./bin/pruebadinamico



